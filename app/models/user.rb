class User < ActiveRecord::Base
  before_validation { self.username = self.username.strip }

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :username, :fname, :lname, presence: true
  validates :username, :fname, :lname, :password, length: { maximum: 50 }
  validates :username, uniqueness: { case_sensitive: false }
  has_secure_password
  validates :password, length: { minimum: 3 }

  def name
    result = [fname, lname].join(" ")
    result
  end
end
