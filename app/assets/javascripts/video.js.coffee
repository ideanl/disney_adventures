$(document).ready( ->
  class g.Playlist
    @videos = []
    constructor: (videos) ->
      @videos = videos

    play: (index = 0, state = null) ->
      if index != @videos.length
        new g.Video(@videos[index])
        videos = @videos
        obj = @
        a = window.setInterval( ->
          if($("##{obj.videos[index]}").length == 0)
            obj.play(index + 1, state)
            clearInterval(a)
        , 100)

      else
        window.g.state = state if state != null

    push: (url) ->
      @videos.push(url)


  class g.Video

    @url = null

    constructor: (url) ->
      @url = url
      @playVideo()

  # autoplay video
    onPlayerReady: (event) ->
      event.target.playVideo()
      return

  # when video ends
    onPlayerStateChange: (event) ->
      if event.data is 0
        $('.player').remove()

      return

    playVideo: ->
      obj = @
      YT.ready( ->
        obj.loadVideo()
      )

    loadVideo: ->
      $('.player_container').html("<div class='player' id='#{@url}'></div>")
      # create youtube player
      player = new YT.Player(@url,
        height: "100%"
        width: "100%"
        playerVars: { 'autoplay': 1, 'controls': 0, 'frameborder': 0, 'showinfo': 0 }
        videoId: @url
        events:
          onReady: @onPlayerReady
          onStateChange: @onPlayerStateChange
      )



  class g.Audio extends Audio
    constructor: (name) ->
      $('body').append("<audio id='#{name}'preload='auto'><source src='/sounds/#{name}.ogg' type='audio/ogg' /><source src='/sounds/#{name}.mp3' type='audio/mpeg' />Your Browser Does Not Support Audio!!!! THIS GAME SUCKS WITHOUT AUDIO</audio>")
      return $("##{name}").get(0)


  class g.AudioPlaylist
    index: 0

    constructor: (names, volume = 1) ->
      $('.audiojs').remove() if $('.audiojs').length > 0
      $('.player_container').prepend('<audio preload></audio>')
      @songs = names

      obj = @
      a = audiojs.createAll(trackEnded: ->
        obj.index++
        if obj.index == obj.songs.length
          obj.index = 0
        audio.load("/sounds/#{obj.songs[obj.index]}.mp3")
        audio.volume = volume
        audio.play()
      )

      audio = a[0]
      audio.load("/sounds/#{@songs[@index]}.mp3")
      audio.setVolume(volume)
      audio.play()

      return audio

)
