$(document).on('ready', ->

  if (window.location.pathname.match(/login/) || window.location.pathname.match(/sessions/))
    songs = new g.AudioPlaylist(['peter_pan', 'friend'], 0.3)

    $('#login_submit').click( ->
        form_array = $('form').serializeArray()
        params = $.param(form_array)
        $.ajax(
          url: "/sessions?#{params}"
          type: 'post'
          beforeSend: (xhr) ->
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
          success: (data) ->
            if data && data['success']
              startIntro()
            else
              $('#flash').html("<div class='alert alert-dismissable alert-danger'><button class='close' type='button' data-dismiss='alert'>&times;</button><p>Invalid Username or Password</p></div>")
              $('form').get(0).reset()
        )
    )

    $('#register').click( ->
      $('#form-container').html(
        "<form action='/users' method='post' id='register_form' class='form-horizontal well trans-bg' style='position:absolute;left:50%; margin-left: -218px; top:50%; margin-top: -169.5px;'>
          <h2 class='text-info text-center'>Register</h2>
          <div class='form-group'>
          <label class='control-label col-md-4' for='user_username'>Username</label>
          <div class='col-md-8'>
          <input class='form-control' id='user_username' name='user[username]' type='text'>
          </div>
          </div>
          <div class='form-group'>
          <label class='control-label col-md-4' for='user_fname'>First Name</label>
          <div class='col-md-8'>
          <input class='form-control' id='user_fname' name='user[fname]' type='text'>
          </div>
          </div>
          <div class='form-group'>
          <label class='control-label col-md-4' for='user_lname'>Last Name</label>
          <div class='col-md-8'>
          <input class='form-control' id='user_lname' name='user[lname]' type='text'>
          </div>
          </div>
          <div class='form-group'>
          <label class='control-label col-md-4' for='user_password'>Password</label>
          <div class='col-md-8'>
          <input class='form-control' id='user_password' name='user[password]' type='password'>
          </div>
          </div>
          <div class='form-group'>
          <label class='control-label col-md-4' for='user_password_confirmation' style='margin-top: -8px;'>Password Confirmation</label>
          <div class='col-md-8'>
          <input class='form-control' id='user_password_confirmation' name='user[password_confirmation]' type='password'>
          </div>
          </div>
          <div class='form-group-submit form-group'>
          <div class='col-md-3 col-md-offset-4'>
          <button class='btn btn-primary' id='register_submit' type='button'>Sign Up</button>
          </div>
          </div>
          </form>
        </div>"
      )
      $('#register_submit').click( ->
        form_array = $('form').serializeArray()
        params = $.param(form_array)
        $.ajax(
          url: "/users?#{params}"
          type: 'post'
          beforeSend: (xhr) ->
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
          success: (data) ->
            if data && data['success'] == 1
              startIntro()
            else
              $('#flash').html("<div class='alert alert-dismissable alert-danger'><button class='close' type='button' data-dismiss='alert'>&times;</button><p>#{data['error']}</p></div>")
        )
      )
    )

    startIntro = () ->
      $('form').html('<h1 class="text-center text-success">User Created</h1><button type="button" class="btn btn-primary center-block" id="start_game">Start Game</button>')
      $('#start_game').click( ->
        elem = $('.player_container').get(0)
        if (elem.requestFullscreen)
          elem.requestFullscreen()
        else if (elem.msRequestFullscreen)
          elem.msRequestFullscreen()
        else if (elem.mozRequestFullScreen)
          elem.mozRequestFullScreen()
        else if (elem.webkitRequestFullscreen)
          elem.webkitRequestFullscreen()

        new g.Intro()

        a = setInterval( ->
          if window.g.state == 'game'
            g.game = new g.Game()
            clearInterval(a)
        , 100)
      )
)
