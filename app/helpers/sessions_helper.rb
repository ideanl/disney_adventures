module SessionsHelper
  
  def require_login
    if !logged_in?
      session[:return_to_url] = request.url if request.get?
      redirect_to login_path
    end
  end

  def login(user)
    @current_user = nil

    old_session = session.dup.to_hash
    old_session.each do |key, value|
      session[key.to_sym] = value
    end

    session[:user_id] = user.id
    current_user
  end

  def current_user
    if session[:user_id] && User.where(id: session[:user_id]).length > 0
      @current_user ||= User.find(session[:user_id]) if session[:user_id]
    else
      @current_user = nil
    end
  end

  def logged_in?
    !!current_user
  end
end
