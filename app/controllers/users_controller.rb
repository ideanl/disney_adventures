class UsersController < ApplicationController
  def new
    unless logged_in?
      @user = User.new
    else
      redirect_to root_url
    end
  end

  def create
    @user = User.new(user_params)

    success = 0
    errors = ''

    if @user.save
      success = 1
      login(@user)
    else
      errors = @user.errors.full_messages.join('. ')
    end

    data = {
      success: success,
      error: errors
    }
    
    render json: data.to_json
  end

  private
    def user_params
      params.require(:user).permit(:username, :fname, :lname, :password, :password_confirmation)
    end
end
