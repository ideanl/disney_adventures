class SessionsController < ApplicationController
  def new
    if logged_in?
      @current_user = nil
      session.clear
    end
  end

  def create
    @user = User.find_by(username: params[:session][:username].downcase)
    success = 0
    if @user && @user.authenticate(params[:session][:password])
      @user = login(@user) if @user
      success = 1
    else
      flash.now[:danger] = "Invalid Username or Password"
    end

    data = {
      success: success
    }

    render json: data.to_json
  end

  def destroy
    @current_user = nil
    session.clear
    flash[:success] = "You have been logged out"
    redirect_to root_url
  end
end
