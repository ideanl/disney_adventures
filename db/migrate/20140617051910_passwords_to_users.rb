class PasswordsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :password, :string
    rename_column :users, :password, :password_digest
  end
end
